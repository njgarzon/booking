#language: es

Característica: : Consulta lista de paises API
  Yo como diseñador front quiero consultar la lista de paises disponibles

  Escenario: : consulta de paises
    Dado que un desarrollador front desea conocer la lista de "paises"
    Cuando consume el api de consulta de "paises"
    Entonces la respuesta de la consulta contiene la lista de "paises"

