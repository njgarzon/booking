#language: es

Característica: : Consulta lista de provincias API
  Yo como diseñador front quiero consultar la lista de provincias disponibles

  Escenario: : consulta de provincias
    Dado que un desarrollador front desea conocer la lista de "provincias"
    Cuando consume el api de consulta de "provincias"
    Entonces la respuesta de la consulta contiene la lista de "provincias"