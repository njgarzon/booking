#language: es
Característica: Busqueda de hotel en booking
  Yo como usuario quiero realizar la reserva de un hotel en booking

  @busquedaDeHotel
  Escenario: Busqueda de hotel
    Dado que yo como usuario quiero realizar la reserva de hotel en booking
    Cuando ingrese los datos
      | goingTo   | checkIn       | checkOut      | travelersAdults | travelersChildren |
      | Cartagena | 17 enero 2022 | 20 enero 2022 | 3               | 0                 |
    Entonces puedo ver la lista de resultados y seleccionar el de menor precio

