#language: es

Característica: Reserva de vuelo en booking
  Yo como usuario de booking quiero reservar un vuelo

  @reservaDeVuelo
  Escenario: Buscar vuelo en booking
    Dado que yo como usuario quiero realizar la reserva de vuelo ingreso a booking
    Cuando ingrese los filtros de vuelo
      | cityFrom | cityGoingTo | departingDate | returningDate |
      | Bogota   | Cartagena   | enero 20   | enero 22   |
    Entonces puedo ver la lista de vuelos