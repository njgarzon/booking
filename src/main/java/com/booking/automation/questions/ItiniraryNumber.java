package com.booking.automation.questions;

import com.booking.automation.userintarfaces.PaymentPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isCurrentlyVisible;

public class ItiniraryNumber implements Question<Boolean> {
    public static ItiniraryNumber confirmed() {
        return new ItiniraryNumber();
    }

    @Override
    public Boolean answeredBy(Actor actor) {

        Logger logger = LoggerFactory.getLogger("Itinerario: ");

        actor.attemptsTo(WaitUntil.the(PaymentPage.LABEL_MESSAGE_TRIP_IS_BOOKED, isCurrentlyVisible())
                .forNoMoreThan(60).seconds());

        logger.info(PaymentPage.LABEL_ITINIRARY_TRAVELOCITY.resolveFor(actor).getText());

        return PaymentPage.LABEL_MESSAGE_TRIP_IS_BOOKED.resolveFor(actor).isCurrentlyVisible() &&
                PaymentPage.LABEL_ITINIRARY_TRAVELOCITY.resolveFor(actor).isCurrentlyVisible();
    }
}
