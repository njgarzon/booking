package com.booking.automation.questions;


import com.booking.automation.userintarfaces.FlightResultsPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;


public class ListOfferFlight implements Question<Boolean> {
    public static ListOfferFlight results() {
        return new ListOfferFlight();
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        return !BrowseTheWeb.as(actor).findAll(FlightResultsPage.XPATH_AIRLINES_FLIGHT_RESULTS).isEmpty();
    }
}
