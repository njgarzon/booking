package com.booking.automation.questions;

import com.booking.automation.userintarfaces.CarOffersResultsPage;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;



public class ListOfferCar implements Question<Boolean> {
    public static ListOfferCar success() {
        return new ListOfferCar();
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        int countCars = BrowseTheWeb.as(actor).findAll(CarOffersResultsPage.XPATH_LIST_OFFER_CAR).size();
        return countCars > 0;
    }
}
