package com.booking.automation.questions;


import com.booking.automation.userintarfaces.SignInTravelocityPage;
import cucumber.api.DataTable;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;

import java.util.List;


public class UserLoggedFailure implements Question<Boolean> {

    private final DataTable msgError;

    public UserLoggedFailure(DataTable nameUser) {
        this.msgError = nameUser;
    }


    public static UserLoggedFailure withMsg(DataTable nameUser) {
        return new UserLoggedFailure(nameUser);
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        List<List<String>> data = msgError.raw();
        boolean result = false;

        List<WebElementFacade> labelsHeader = BrowseTheWeb.as(actor).findAll(SignInTravelocityPage.XPATH_LABEL_MSG_ERROR);
        for (WebElementFacade webElementFacade : labelsHeader) {
            String a = webElementFacade.getText();
            if (a.equals(data.get(1).get(0))) {
                result = true;
                break;
            }
        }

        return result;
    }
}
