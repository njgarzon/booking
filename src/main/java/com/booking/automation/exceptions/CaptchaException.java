package com.booking.automation.exceptions;

public class CaptchaException extends AssertionError {

    public CaptchaException(String mensaje) {
        super(mensaje);
    }
}
