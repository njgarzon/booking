package com.booking.automation.userintarfaces;

import net.serenitybdd.screenplay.targets.Target;

public class FlightResultsPage {

    public static final String XPATH_AIRLINES_FLIGHT_RESULTS = "//div[@class='resultInner']";
    public static final Target LABEL_FILTER_ORDER = Target.the("Filtro de orden")
            .locatedBy("//label[@for='listings-sort']");
    public static final String XPATH_BUTTON_SELECT_FLIGTH = "//button[@data-test-id='select-link']";
    public static final Target BUTTON_ADD_HOTEL_NO = Target.the("Boton Añadir hotel no gracias")
            .locatedBy("//span[@class='no-thanks-content']");
    public static final Target LABEL_TRIP_SUMMARY = Target.the("Resumen de vuelo")
            .locatedBy("//button[@data-test-id='fare-type-select']");

    private FlightResultsPage() {
    }
}
