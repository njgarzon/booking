package com.booking.automation.userintarfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class SignInTravelocityPage {

    public static final Target TEXT_EMAIL = Target.the("Campo Email")
            .located(By.id("username"));
    public static final Target TEXT_PASSWORD = Target.the("Campo Contraseña")
            .located(By.xpath("//input[@type='password']"));
    public static final Target BUTTON_CONTINUE_EMAIL = Target.the("Boton inicio de sesion")
            .located(By.xpath("//button[@type='submit']"));

    public static final Target BUTTON_SIGN_IN = Target.the("Boton inicio de sesion")
            .located(By.xpath("//button[@type='submit']"));

    public static final String XPATH_LABEL_MSG_ERROR = "//div[@role='alert']";

    private SignInTravelocityPage() {
    }
}
