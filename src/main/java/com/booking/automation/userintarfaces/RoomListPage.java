package com.booking.automation.userintarfaces;

public class RoomListPage {

    public static final String XPATH_ROOM_LIST_RESERVE = "//button[@data-stid='submit-hotel-reserve']";

    private RoomListPage() {
    }
}
