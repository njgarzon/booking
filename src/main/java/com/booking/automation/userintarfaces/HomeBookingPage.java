package com.booking.automation.userintarfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class HomeBookingPage {

    public static final Target LINK_SING_IN = Target.the("Link Sign in")
            .locatedBy("//button[text()='Sign in']");
    public static final Target BUTTON_SIGN_IN = Target.the("Boton iniciar sesion")
            .locatedBy("//a[@class='bui-button bui-button--secondary js-header-login-link']/span[contains(text(),'Inicia sesión')]");
    public static final String XPATH_LABEL_HEADER = "//span[@id='profile-menu-trigger--title']";
    public static final Target RECAPTCHA = Target.the("Recaptcha")
            .locatedBy("//iframe[@title='recaptcha challenge']");
    public static final Target BUTTON_DESTINATION_HOTEL = Target.the("Texto going to")
            .locatedBy("//input[@id='ss']");
    public static final Target TEXT_DESTINATION_HOTEL = Target.the("Texto destino")
            .located(By.id("ss"));
    public static final String XPATH_LIST_RESULTS_DESTINATIONS = "//ul[@aria-label='Lista de destinos recomendados']";
    public static final Target CHECK_IN_DATE = Target.the("Fecha check in").locatedBy("//span[@class='sb-date-field__icon sb-date-field__icon-btn bk-svg-wrapper calendar-restructure-sb']");
    public static final String XPATH_DAYS_CALENDAR = "//span[contains(@role, 'checkbox')]";
    public static final String XPATH_DAYS_CALENDAR_FLIGHT = "//div[@data-val and @aria-label]";
    public static final Target TEXT_TRAVELERS = Target.the("Datos viajeros")
            .locatedBy("//label[@id='xp__guests__toggle']");
    public static final Target LABEL_ADULTS = Target.the("Cantidad de adultos")
            .locatedBy("//div[@class='sb-group__field sb-group__field-adults']//span[@class='bui-stepper__display']");
    public static final Target LABEL_CHILDREN = Target.the("Cantidad de niños")
            .locatedBy("//div[@class='sb-group__field sb-group-children ']//span[@class='bui-stepper__display']");
    public static final Target BUTTON_INCREASE_ADULTS = Target.the("Boton incrementar adultos")
            .locatedBy("//button[@aria-label='Aumenta el número de Adultos']");
    public static final Target BUTTON_INCREASE_CHILDREN = Target.the("Boton incrementar niños")
            .locatedBy("//button[@aria-label='Aumenta el número de Niños']");
    public static final Target BUTTON_DONE_TRAVELERS = Target.the("Boton done travelers")
            .locatedBy("//button[@data-testid='guests-done-button']");
    public static final Target BUTTON_SEARCH_HOME = Target.the("Boton search en el home")
            .locatedBy("//button[@data-sb-id='main']");
    public static final Target BUTTON_SEARCH_CARS = Target.the("Boton search en el home")
            .locatedBy("//button[@class=\"sb-searchbox__button \"]");
    public static final Target BUTTON_ORIGIN_FLIGHT = Target.the("Boton origen")
            .locatedBy("//div[@class='_ia1']");
    public static final Target TEXT_ORIGIN_FLIGTH = Target.the("Campo Origen vuelo")
            .locatedBy("//input[contains(@id,'origin-airport')]");
    public static final Target BUTTON_DESTINATION_FLIGHT = Target.the("Boton destino")
            .locatedBy("//div[contains(@id, 'destination-input-wrapper')]");
    public static final Target TEXT_DESTINATION_FLIGTH = Target.the("Campo destino vuelo")
            .locatedBy("//input[contains(@id,'destination-airport')]");
    public static final Target BUTTON_TRAVELERS_FLIGHT = Target.the("Link de viajeros")
            .locatedBy("//form[@aria-hidden='false']//button[@aria-label='Buscar vuelos']");
    public static final Target LINK_PREFERRED_CLASS = Target.the("Link clase preferidad")
            .located(By.id("preferred-class-input"));
    public static final String XPATH_PREFERRED_CLASS = "//a[@class='uitk-list-item']";
    public static final String XPATH_ORIGINS_FLIGHT_RESULTS = "//div[@class='item-info']";
    public static final String XPATH_DESTINATIONS_RESULTS = "//div[contains(@id,'destination-airport-smarty-content')]//div[@class='item-info']";

    public static final Target TEXT_PICK_UP_CITY = Target.the("Ciudad de recogida del vehiculo")
            .located(By.id("ss_origin"));
    public static final String XPATH_PICK_UP_RESULTS = "//ul[@class='c-autocomplete__list sb-autocomplete__list -visible']//li";


    public static final String XPATH_SELECT_CHILD_AGE = "//section//select[@id='child-age-input-0-%s']";

    public static final Target CARS = Target.the("Seccion alquiler de coches")
            .locatedBy("//span[contains(text(),'Alquiler de coches')] ");

    public static final Target FLIGHT = Target.the("Seccion alquiler de coches")
            .locatedBy("//span[contains(text(),'Vuelos')]");


    public static final Target CHECK_IN_DATE_FLIGHT = Target.the("Fecha de ida").locatedBy("//div[contains(@id, 'dateRangeInput-display-start') and @aria-label='Fecha de ida']");
    public static final Target XPATH_CHECK_IN_DATE_FLIGHT = Target.the("Fecha de ida").locatedBy("//div[contains(@id, 'dateRangeInput-display-start') and @aria-label='Fecha de ida']");
    public static final Target CHECK_OUT_DATE_FLIGHT = Target.the("Fecha de vuelta").locatedBy("//div[contains(@id, 'dateRangeInput-display-end') and @aria-label='Fecha de vuelta']");
    public static final Target CHECK_IN_DATE_CARS = Target.the("Fecha check in").locatedBy("//div[@data-mode='checkin']");
    public static final Target CHECK_OUT_DATE_CARS = Target.the("Fecha check out").locatedBy("//div[@data-mode='checkout']");
    public static final String XPATH_DAYS_CALENDAR_CARS= "//div[2]/div[@class='xp__dates-inner xp__dates__checkin']//td[@data-id]";




    private HomeBookingPage() {
    }
}
