package com.booking.automation.userintarfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class HotelsResultsPage {

    public static final String XPATH_LIST_RESULTS = "//div[@data-testid='property-card']";
    public static final Target BUTTON_LOWPRICE = Target.the("Lista de ordenamiento")
            .locatedBy("//li[@data-id='price']");
    private HotelsResultsPage() {
    }
}
