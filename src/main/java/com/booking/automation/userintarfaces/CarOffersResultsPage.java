package com.booking.automation.userintarfaces;

public class CarOffersResultsPage {

    public static final String XPATH_LIST_OFFER_CAR = "//div[@class='bui-panel   carResultDiv']";

    private CarOffersResultsPage() {
    }
}
