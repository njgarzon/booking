package com.booking.automation.models;

import cucumber.api.DataTable;

import java.util.List;
import java.util.Map;

public class FiltersFlight {

    private final String cityFrom;
    private final String cityGoingTo;
    private final String departingDate;
    private final String returningDate;



    public String getCityFrom() {
        return cityFrom;
    }

    public String getCityGoingTo() {
        return cityGoingTo;
    }

    public String getDepartingDate() {
        return departingDate;
    }

    public String getReturningDate() {
        return returningDate;
    }

    public FiltersFlight(DataTable filters) {
        List<Map<String, String>> filter = filters.asMaps(String.class, String.class);
        this.cityFrom = filter.get(0).get("cityFrom");
        this.cityGoingTo = filter.get(0).get("cityGoingTo");
        this.departingDate = filter.get(0).get("departingDate");
        this.returningDate = filter.get(0).get("returningDate");
    }
}
