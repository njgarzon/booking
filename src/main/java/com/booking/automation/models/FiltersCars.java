package com.booking.automation.models;

import cucumber.api.DataTable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class FiltersCars {

    private final String pickUpCity;
    private final String dropOffCity;
    private final String pickUpDate;
    private final String dropOffDate;
    private final String pickupTime;
    private final String dropOffTime;

    public String getPickUpCity() {
        return pickUpCity;
    }

    public String getDropOffCity() {
        return dropOffCity;
    }

    public String getPickUpDate() {
        return pickUpDate;
    }

    public Long getPickUpDateAsLong(){
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:ss a dd-MMM-yyyy");
            Date parseDate = dateFormat.parse("00:00:00 AM "+pickUpDate);
            return parseDate.getTime() - 18000000;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Long getDropOffDateAsLong(){
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:ss a dd-MMM-yyyy");
            Date parseDate = dateFormat.parse("00:00:00 AM "+dropOffDate);
            return parseDate.getTime() - 18000000;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getDropOffDate() {
        return dropOffDate;
    }

    public String getPickupTime() {
        return pickupTime;
    }

    public String getDropOffTime() {
        return dropOffTime;
    }

    public FiltersCars(DataTable filters) {
        List<Map<String, String>> filter = filters.asMaps(String.class, String.class);
        this.pickUpCity = filter.get(0).get("pickUpCity");
        this.dropOffCity = filter.get(0).get("dropOffCity");
        this.pickUpDate = filter.get(0).get("pickUpDate");
        this.dropOffDate = filter.get(0).get("dropOffDate");
        this.pickupTime = filter.get(0).get("pickupTime");
        this.dropOffTime = filter.get(0).get("dropOffTime");
    }
}
