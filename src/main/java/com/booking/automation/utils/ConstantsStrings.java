package com.booking.automation.utils;

public class ConstantsStrings {

    public static final String ACTOR_NAME = "Nelson";
    public static final String URL_HOME = "https://www.booking.com/";
    public static final String CAPTCHA_MESSAGE = "Captcha presente, no se puede completar login";
    public static final String ATTRIBUTE_ARIA_LABEL = "aria-label";
    public static final String PRICE_LOW_TO_HIGH = "PRICE_LOW_TO_HIGH";
    public static final String FLIGHTS = "Flights";
    public static final String XPATH_OPTIONS_HEADER = "//li[@class='bui-tab__item']";
    public static final String AIRLINE = "airLine";
    public static final String SAME_AS_PICK_UP = "Same as pick-up";
    public static final String ENDPOINT = "https://www.travelocity.com";
    public static final String PATH_COUNTRIES_LIST = "/site/countrylist";
    public static final String PATH_PROVINCE_LIST = "/site/provincelist";
    public static final String ATTRIBUTE_DATA_ID = "data-id";

    private ConstantsStrings() {
    }
}
