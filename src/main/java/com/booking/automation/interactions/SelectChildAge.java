package com.booking.automation.interactions;

import com.booking.automation.userintarfaces.HomeBookingPage;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;

import net.serenitybdd.screenplay.actions.SelectFromOptions;

import java.util.List;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class SelectChildAge implements Interaction {

    private final String ageSelected;

    public SelectChildAge(String ageSelected) {this.ageSelected = ageSelected;}

    public static SelectChildAge age(String ageSelected) {
        return instrumented(SelectChildAge.class, ageSelected);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        List<WebElementFacade> selectAge = BrowseTheWeb.as(actor).findAll(String.format(HomeBookingPage.XPATH_SELECT_CHILD_AGE,ageSelected));
        actor.attemptsTo(SelectFromOptions.byValue(ageSelected).from(selectAge.get(0)));
    }
}
