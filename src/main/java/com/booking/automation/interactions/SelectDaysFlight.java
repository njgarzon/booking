package com.booking.automation.interactions;

import com.booking.automation.userintarfaces.HomeBookingPage;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Click;

import java.util.List;

import static com.booking.automation.utils.ConstantsStrings.ATTRIBUTE_ARIA_LABEL;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class SelectDaysFlight implements Interaction {

    private final String daySelected;

    public SelectDaysFlight(String daySelected) {
        this.daySelected = daySelected;
    }

    public static SelectDaysFlight calendar(String daySelected) {
        return instrumented(SelectDaysFlight.class, daySelected);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        List<WebElementFacade> listDays = BrowseTheWeb.as(actor).findAll(HomeBookingPage.XPATH_DAYS_CALENDAR_FLIGHT);

        for (WebElementFacade checkIntoCalendar : listDays) {
            if (checkIntoCalendar.getAttribute(ATTRIBUTE_ARIA_LABEL).contains(daySelected)) {
                actor.attemptsTo(Click.on(checkIntoCalendar));
                break;
            }
        }
    }
}
