package com.booking.automation.interactions;

import com.booking.automation.userintarfaces.HomeBookingPage;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import org.openqa.selenium.JavascriptExecutor;

import java.util.List;


import static com.booking.automation.utils.ConstantsStrings.ATTRIBUTE_DATA_ID;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class SelectDaysCars implements Interaction {

    private final String daySelected;

    public SelectDaysCars(String daySelected) {
        this.daySelected = daySelected;
    }

    public static SelectDaysCars calendar(String daySelected) {
        return instrumented(SelectDaysCars.class, daySelected);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        List<WebElementFacade> listDays = BrowseTheWeb.as(actor).findAll(HomeBookingPage.XPATH_DAYS_CALENDAR_CARS);

        for (WebElementFacade checkIntoCalendar : listDays) {
            if (checkIntoCalendar.getAttribute(ATTRIBUTE_DATA_ID).contains(daySelected)) {
                JavascriptExecutor executor = (JavascriptExecutor)  BrowseTheWeb.as(actor).getDriver();
                executor.executeScript("arguments[0].click();", checkIntoCalendar);
                   // actor.attemptsTo(Click.on(checkIntoCalendar));
                    break;

            }
        }
    }
}
