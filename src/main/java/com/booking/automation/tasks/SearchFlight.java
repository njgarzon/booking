package com.booking.automation.tasks;

import com.booking.automation.interactions.GoTo;
import com.booking.automation.interactions.SelectDaysFlight;
import com.booking.automation.interactions.SelectDaysInto;
import com.booking.automation.interactions.SelectFirstResult;
import com.booking.automation.models.FiltersFlight;
import com.booking.automation.userintarfaces.FlightResultsPage;
import com.booking.automation.userintarfaces.HomeBookingPage;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.waits.WaitUntil;

import java.util.List;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SearchFlight implements Task {

    private final FiltersFlight filtersFlight;

    public SearchFlight(FiltersFlight filtersFlight) {
        this.filtersFlight = filtersFlight;
    }

    public static SearchFlight with(FiltersFlight filtersFligth) {
        return instrumented(SearchFlight.class, filtersFligth);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(HomeBookingPage.FLIGHT),
                Click.on(HomeBookingPage.BUTTON_ORIGIN_FLIGHT),
                Enter.theValue(filtersFlight.getCityFrom()).into(HomeBookingPage.TEXT_ORIGIN_FLIGTH),
                SelectFirstResult.onList(HomeBookingPage.XPATH_ORIGINS_FLIGHT_RESULTS),
                Click.on(HomeBookingPage.BUTTON_DESTINATION_FLIGHT),
                Enter.theValue(filtersFlight.getCityGoingTo()).into(HomeBookingPage.TEXT_DESTINATION_FLIGTH),
                SelectFirstResult.onList(HomeBookingPage.XPATH_DESTINATIONS_RESULTS),
                Click.on(HomeBookingPage.CHECK_IN_DATE_FLIGHT),
                SelectDaysFlight.calendar(filtersFlight.getDepartingDate()),
                SelectDaysFlight.calendar(filtersFlight.getReturningDate()),
                Click.on(HomeBookingPage.BUTTON_TRAVELERS_FLIGHT));
    }
}
