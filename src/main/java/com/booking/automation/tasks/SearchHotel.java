package com.booking.automation.tasks;

import com.booking.automation.interactions.SelectDaysInto;
import com.booking.automation.interactions.SelectFirstResult;
import com.booking.automation.models.FiltersHotel;
import com.booking.automation.userintarfaces.HomeBookingPage;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

import java.util.List;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class SearchHotel implements Task {

    private final FiltersHotel filtersHotel;

    public SearchHotel(FiltersHotel filtersHotel) {
        this.filtersHotel = filtersHotel;
    }

    public static SearchHotel with(FiltersHotel filtersHotel) {
        return instrumented(SearchHotel.class, filtersHotel);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(HomeBookingPage.BUTTON_DESTINATION_HOTEL),
                Enter.theValue(filtersHotel.getGoingTo()).into(HomeBookingPage.TEXT_DESTINATION_HOTEL),
                SelectFirstResult.onList(HomeBookingPage.XPATH_LIST_RESULTS_DESTINATIONS),
                Click.on(HomeBookingPage.CHECK_IN_DATE));

        actor.attemptsTo(
                Click.on(HomeBookingPage.CHECK_IN_DATE),
                SelectDaysInto.calendar(filtersHotel.getCheckIn()),
                SelectDaysInto.calendar(filtersHotel.getCheckOut()),
                Click.on(HomeBookingPage.TEXT_TRAVELERS));

        while (!HomeBookingPage.LABEL_ADULTS.resolveFor(actor).getText()
                .equals(filtersHotel.getTravelersAdults())) {
            actor.attemptsTo(Click.on(HomeBookingPage.BUTTON_INCREASE_ADULTS));
        }


        while (!HomeBookingPage.LABEL_CHILDREN.resolveFor(actor).getText()
                .equals(filtersHotel.getTravelersChildren())) {
            actor.attemptsTo(Click.on(HomeBookingPage.BUTTON_INCREASE_CHILDREN));
        }

        for(int i=0; i<Integer.parseInt(filtersHotel.getTravelersChildren()); i++){
            List<WebElementFacade> selectAge = BrowseTheWeb.as(actor).findAll(String.format(HomeBookingPage.XPATH_SELECT_CHILD_AGE,i));
            actor.attemptsTo(SelectFromOptions.byValue((i+1)+"").from(selectAge.get(0)));
        }

        actor.attemptsTo(Click.on(HomeBookingPage.BUTTON_SEARCH_HOME));
    }
}
