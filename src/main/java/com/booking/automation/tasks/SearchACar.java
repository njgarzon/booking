package com.booking.automation.tasks;

import com.booking.automation.interactions.SelectDaysCars;
import com.booking.automation.interactions.SelectDaysInto;
import com.booking.automation.interactions.SelectFirstResult;
import com.booking.automation.models.FiltersCars;
import com.booking.automation.userintarfaces.HomeBookingPage;
import com.booking.automation.utils.ConstantsStrings;
import com.booking.automation.userintarfaces.CarOffersResultsPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SearchACar implements Task {

    private final FiltersCars filtersCars;

    public SearchACar(FiltersCars filtersCars) {
        this.filtersCars = filtersCars;
    }

    public static SearchACar with(FiltersCars filtersCars) {
        return instrumented(SearchACar.class, filtersCars);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(HomeBookingPage.CARS),
                Enter.theValue(filtersCars.getPickUpCity()).into(HomeBookingPage.TEXT_PICK_UP_CITY),
                SelectFirstResult.onList(HomeBookingPage.XPATH_PICK_UP_RESULTS));

        actor.attemptsTo(
                Click.on(HomeBookingPage.CHECK_IN_DATE_CARS),
                SelectDaysCars.calendar(filtersCars.getPickUpDateAsLong()+""),
                Click.on(HomeBookingPage.CHECK_OUT_DATE_CARS),
                SelectDaysCars.calendar(filtersCars.getDropOffDateAsLong()+""));

        actor.attemptsTo(
                Click.on(HomeBookingPage.BUTTON_SEARCH_CARS));
    }
}
