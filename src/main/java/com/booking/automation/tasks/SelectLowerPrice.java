package com.booking.automation.tasks;

import com.booking.automation.userintarfaces.HotelsResultsPage;
import com.booking.automation.utils.ConstantsStrings;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class SelectLowerPrice implements Task {
    public static SelectLowerPrice ofResultList() {
        return instrumented(SelectLowerPrice.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(HotelsResultsPage.BUTTON_LOWPRICE)
        );
    }
}
