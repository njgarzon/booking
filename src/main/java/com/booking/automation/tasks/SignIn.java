package com.booking.automation.tasks;

import com.booking.automation.models.Credentials;
import com.booking.automation.userintarfaces.HomeBookingPage;
import com.booking.automation.userintarfaces.SignInTravelocityPage;
import cucumber.api.DataTable;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class SignIn implements Task {

    private final DataTable credentials;

    public SignIn(DataTable credentials) {
        this.credentials = credentials;
    }

    public static SignIn now(DataTable credentials) {
        return instrumented(SignIn.class, credentials);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        new Credentials(credentials);

        actor.attemptsTo(
                Click.on(HomeBookingPage.BUTTON_SIGN_IN),
                Enter.theValue(Credentials.getEmail()).into(SignInTravelocityPage.TEXT_EMAIL),
                Click.on(SignInTravelocityPage.BUTTON_SIGN_IN),
                Enter.theValue(Credentials.getPassword()).into(SignInTravelocityPage.TEXT_PASSWORD),
                Click.on(SignInTravelocityPage.BUTTON_SIGN_IN)
        );
    }
}
