package com.booking.automation.stepdefinitions;

import com.booking.automation.interactions.GoTo;

import com.booking.automation.models.FiltersFlight;
import com.booking.automation.questions.HotelList;
import com.booking.automation.questions.ListOfferFlight;
import com.booking.automation.questions.TripSummary;
import com.booking.automation.tasks.SearchFlight;
import com.booking.automation.utils.ConstantsStrings;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;

import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class ReserveFlightStepsDefinitions {

    @Before
    public void setUp() {
        OnStage.setTheStage(new OnlineCast());
    }

    Logger logger = LoggerFactory.getLogger(getClass());

    @Dado("^que yo como usuario quiero realizar la reserva de vuelo ingreso a booking$")
    public void queYoComoUsuarioQuieroRealizarLaReservaDeVueloIngresoABooking() {
        theActorCalled(ConstantsStrings.ACTOR_NAME).wasAbleTo(Open.url(ConstantsStrings.URL_HOME));
    }

    @Cuando("^ingrese los filtros de vuelo$")
    public void ingreseLosFiltrosDeVuelo(DataTable dataTable) {
        FiltersFlight filters = new FiltersFlight(dataTable);
        theActorInTheSpotlight().attemptsTo(
                GoTo.option(ConstantsStrings.FLIGHTS),
                SearchFlight.with(filters));
    }

    @Entonces("^puedo ver la lista de vuelos$")
    public void puedoVerLaListaDeVuelos() {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(ListOfferFlight.results()));

    }

}
