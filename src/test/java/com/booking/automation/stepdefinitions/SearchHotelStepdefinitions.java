package com.booking.automation.stepdefinitions;


import com.booking.automation.models.FiltersHotel;
import com.booking.automation.questions.HotelList;
import com.booking.automation.tasks.SearchHotel;
import com.booking.automation.tasks.SelectLowerPrice;
import com.booking.automation.utils.ConstantsStrings;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class SearchHotelStepdefinitions {


    @Before
    public void setUp() {
        OnStage.setTheStage(new OnlineCast());
    }

    @Dado("^que yo como usuario quiero realizar la reserva de hotel en booking$")
    public void queYoComoUsuarioQuieroRealizarLaReservaDeHotelEnBooking() {
        theActorCalled(ConstantsStrings.ACTOR_NAME).wasAbleTo(Open.url(ConstantsStrings.URL_HOME));
    }


    @Cuando("^ingrese los datos$")
    public void ingreseLosFiltros(DataTable dataTable) {
        FiltersHotel filtersHotel = new FiltersHotel(dataTable);
        theActorInTheSpotlight().attemptsTo(SearchHotel.with(filtersHotel));

    }

    @Entonces("^puedo ver la lista de resultados y seleccionar el de menor precio$")
    public void puedoVerLaListaDeResultadosYSeleccionarElDeMenorPrecio() {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(HotelList.results()));

    }

}
