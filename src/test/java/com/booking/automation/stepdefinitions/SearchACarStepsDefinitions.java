package com.booking.automation.stepdefinitions;

import com.booking.automation.interactions.GoTo;
import com.booking.automation.models.FiltersCars;
import com.booking.automation.questions.ListOfferCar;
import com.booking.automation.tasks.SearchACar;
import com.booking.automation.utils.ConstantsStrings;
import cucumber.api.DataTable;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.GivenWhenThen;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class SearchACarStepsDefinitions {

    @Cuando("^ingreso los datos de la renta$")
    public void ingresoLosDatosDeLaRenta(DataTable dataTable) {
        FiltersCars filtersCars = new FiltersCars(dataTable);

        theActorInTheSpotlight().attemptsTo(
                SearchACar.with(filtersCars));
    }

    @Entonces("^puedo ver lista de vehiculos$")
    public void puedoVerListaDeVehiculos() {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(ListOfferCar.success()));

    }
}
