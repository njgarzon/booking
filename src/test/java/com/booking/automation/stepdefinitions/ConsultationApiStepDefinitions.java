package com.booking.automation.stepdefinitions;

import com.booking.automation.utils.ConstantsStrings;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.interactions.Get;
import net.thucydides.core.util.EnvironmentVariables;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;

public class ConsultationApiStepDefinitions {
    private String theRestApiBaseUrl;
    private EnvironmentVariables environmentVariables;
    private String path = "";


    @Dado("^que un desarrollador front desea conocer la lista de \"([^\"]*)\"$")
    public void queUnDesarrolladorFrontDeseaConocerLaListaDe(String arg1) {
        theRestApiBaseUrl = environmentVariables.optionalProperty("travelocity.baseurl")
                .orElse(ConstantsStrings.ENDPOINT);
        theActorCalled(ConstantsStrings.ACTOR_NAME).whoCan(CallAnApi.at(theRestApiBaseUrl));
        if (arg1.equals("paises")) path = ConstantsStrings.PATH_COUNTRIES_LIST;
        else path = ConstantsStrings.PATH_PROVINCE_LIST;
    }

    @Cuando("^consume el api de consulta de \"([^\"]*)\"$")
    public void consumeElApiDeConsultaDe(String arg1) {
        theActorInTheSpotlight().attemptsTo(Get.resource(path).with(request -> request.header("Cookie", "CRQS=t|80001`s|80001`l|en_US`c|USD; CRQSS=e|0; DUAID=8df1fc7d-21de-4fdb-9d59-60be8454ad63; HMS=7889803e-a99d-4eec-993a-5f1e1140026a; JSESSION=b4c242d1-bb06-4e27-a2d6-a9f31999bf9c; MC1=GUID=8df1fc7d21de4fdb9d5960be8454ad63; ak_bmsc=1CF9F8549099C65992047D46285850DFB531FF04740B0000C3B9B2606FFBCF02~pl+PkEJyPPyBD+jhF2eP2xLjcjvm3Ewjr0P6myEknLjQITv3ma66+I214OFzMOKPn8ZN5sAfVJxyLcnOtYYMRY11v8d8EExtnzNUHVNMUzn5O3FeZOGCU7/WGAOXEdLTo/VKVHF3/zKHO8F31/jv8RbAXKrJtTUkpN1OyFs8DM0VH0NK29eM9g/c7/doIm/zBpbsceqNnCC1l5k9RK1nTJWW6eYIwGYIRaj4sr83WlNhs=; cesc=%7B%22marketingClick%22%3A%5B%22true%22%2C1622325699487%5D%2C%22hitNumber%22%3A%5B%221%22%2C1622325699487%5D%2C%22visitNumber%22%3A%5B%221%22%2C1622325699487%5D%2C%22cidVisit%22%3A%5B%22Brand.DTI%22%2C1622325699487%5D%2C%22entryPage%22%3A%5B%22Homepage%22%2C1622325699487%5D%2C%22cid%22%3A%5B%22Brand.DTI%22%2C1622325699487%5D%7D; currency=USD; iEAPID=0; linfo=v.4,|0|0|255|1|0||||||||1033|0|0||0|0|0|-1|-1; tpid=v.1,80001")));
    }

    @Entonces("^la respuesta de la consulta contiene la lista de \"([^\"]*)\"$")
    public void laRespuestaDeLaConsultaContieneLaListaDe(String arg1) {
        theActorInTheSpotlight().should(seeThatResponse("La api entrego código exitoso", response
                -> response.statusCode(200)));
    }
}
