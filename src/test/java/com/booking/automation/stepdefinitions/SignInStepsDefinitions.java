package com.booking.automation.stepdefinitions;

import com.booking.automation.questions.UserLogged;
import com.booking.automation.questions.UserLoggedFailure;
import com.booking.automation.tasks.SignIn;
import com.booking.automation.utils.ConstantsStrings;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class SignInStepsDefinitions {

    @Before
    public void setUp() {
        OnStage.setTheStage(new OnlineCast());
    }

    @Dado("^que yo como usuario quiero iniciar sesion en booking con correo$")
    public void queYoComoUsuarioQuieroIniciarSesionEnBookingConCorreo() {
        theActorCalled(ConstantsStrings.ACTOR_NAME).wasAbleTo(Open.url(ConstantsStrings.URL_HOME));

    }

    @Cuando("^ingrese mis credenciales correo y password$")
    public void ingreseMisCredencialesY(DataTable credenciales) {
        theActorInTheSpotlight().attemptsTo(SignIn.now(credenciales));
    }

    @Entonces("^puedo ver el usuario logueado$")
    public void puedoVerElUsuarioNombreLogueado(DataTable nombre) {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(UserLogged.withName(nombre)));
    }

    @Entonces("^puedo ver el mensaje de error$")
    public void puedoVerElMensajeDeError(DataTable mensaje) {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(UserLoggedFailure.withMsg(mensaje)));
    }

    @Dado("^que yo quiero realizar una operacion en booking$")
    public void queYoQuieroRealizarUnaOperacionEnBooking() {
        theActorCalled(ConstantsStrings.ACTOR_NAME).wasAbleTo(Open.url(ConstantsStrings.URL_HOME));
    }
}
