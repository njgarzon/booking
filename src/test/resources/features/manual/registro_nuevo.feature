#language: es
Característica: Registro en booking
  Yo como usuario requiero hacer registro de nuevo usuario en Traveolcity


  Escenario: Registro de nuevo usuario en booking con correo exitoso
    Dado que yo como usuario quiero hacer registro de una nueva cuenta con correo
    Cuando ingrese mis datos correctamente
    Entonces puedo ver el mensaje de registro exitoso

  Escenario: Registro de nuevo usuario en booking con facebook exitoso
    Dado que yo como usuario quiero hacer registro de una nueva cuenta con facebook
    Cuando ingrese mis datos correctamente
    Entonces puedo ver el mensaje de registro exitoso

  Escenario: Registro de nuevo usuario en booking con cuenta apple exitoso
    Dado que yo como usuario quiero hacer registro de una nueva cuenta con cuenta apple
    Cuando ingrese mis datos correctamente
    Entonces puedo ver el mensaje de registro exitoso

  Escenario: Registro de nuevo usuario en booking con correo fallido
    Dado que yo como usuario quiero hacer registro de una cuenta con correo ya registrado
    Cuando ingrese mis datos correctamente
    Entonces puedo ver el mensaje de registro ya existe


