#language: es
Característica: Navegar en home de booking
  Yo como usuario requiero navegar en el home de Booking

  Antecedentes:
    Dado que yo como usuario quiero navegar en la pagina booking

  Escenario: Cambiar a idioma español
    Cuando ingrese a la pagina y selecciono idioma español en el encabezado
    Entonces puedo ver todo el sitio en idioma español

  Escenario: Validar seccion Descubre colombia
    Cuando vea la seccion "Descubre colombia"
    Entonces puedo ver las imagenes con las dimensiones correctas y alta definicion

  Escenario: Validar seccion Inspírate para tu próximo viaje
    Cuando vea la seccion "Inspírate para tu próximo viaje"
    Entonces puedo ver las imagenes con las dimensiones correctas y alta definicion

  Escenario: Validar seccion Casas que encantan a los clientes
    Cuando vea la seccion "Casas que encantan a los clientes"
    Entonces puedo ver las imagenes con las dimensiones correctas y alta definicion

  Escenario: Validar seccion Conecta con gente viajera
    Cuando vea la seccion "Conecta con gente viajera"
    Entonces puedo ver las imagenes con las dimensiones correctas y alta definicion

  Escenario: Validar seccion Busca por tipo de alojamiento
    Cuando vea la seccion "Busca por tipo de alojamiento"
    Entonces puedo ver las imagenes con las dimensiones correctas y alta definicion

  Escenario: Validar seccion Destinos que más nos gustan
    Cuando vea la seccion "Destinos que más nos gustan"
    Entonces puedo ver las opciones de regiones, ciudades y lugares de interés

  Escenario: Validar seccion Destinos que más nos gustan opcion regiones
    Cuando vea la seccion "Destinos que más nos gustan"
    Y seleccione la opcion "regiones"
    Entonces puedo ver el listado de regiones
    Y cantidad de alojamientos por region

  Escenario: Validar seccion Destinos que más nos gustan opcion ciudades
    Cuando vea la seccion "Destinos que más nos gustan"
    Y seleccione la opcion "ciudades"
    Entonces puedo ver el listado de regiones
    Y cantidad de alojamientos por ciudad

  Escenario: Validar seccion Destinos que más nos gustan opcion lugares de interés
    Cuando vea la seccion "Destinos que más nos gustan"
    Y seleccione la opcion "lugares de interés"
    Entonces puedo ver el listado de regiones
    Y cantidad de alojamientos por lugar


  Esquema del escenario: Validar mensaje plan de viaje afectado por COVID-19
    Cuando ingrese a cualquier opcion de menu <opcion>
    Entonces puedo ver el mensaje de vaijes afectados por COVID-19

    Ejemplos:
      | opcion          |
      | Paquetes        |
      | Hoteles         |
      | Autos           |
      | Vuelos          |
      | Curceros        |
      | Cosas por hacer |




