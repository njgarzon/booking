#language: es
Característica: Inicio de sesion en booking
  Yo como usuario requiero iniciar sesion con usuario registrado en booking

  @inicioDeSesionCorreo
  Escenario: Inicio de sesion exitoso con correo
    Dado que yo como usuario quiero iniciar sesion en booking con correo
    Cuando ingrese mis credenciales correo y password
      | correo               | password        |
      | nj_garzon@hotmail.com| Nels0nj@v1er    |
    Entonces puedo ver el usuario logueado
      | nombre |
      | Tu cuenta |

  @inicioDeSesionCorreoFallido
  Escenario: Inicio de sesion no exitoso con correo
    Dado que yo como usuario quiero iniciar sesion en booking con correo
    Cuando ingrese mis credenciales correo y password
      | correo               | password        |
      | nj_garzon@hotmail.com| clave errada    |
    Entonces puedo ver el mensaje de error
      | mensaje |
      | La combinación de e-mail y contraseña que has introducido no coinciden |