#language: es

Característica: Buscar vehiculo en booking
  Yo como usuario de travelocity quiero rentar un vehiculo

  Antecedentes: iniciar sesion
    Dado que yo quiero realizar una operacion en booking

  @busquedaVehiculo
  Escenario: Buscar un vehiculo en booking
    Cuando ingreso los datos de la renta
      | pickUpCity | dropOffCity     | pickUpDate | dropOffDate | pickupTime | dropOffTime |
      | Brooklyn   | Same as pick-up | 20-ene-2022 | 22-ene-2022  | 0830AM     | 0300PM      |
    Entonces puedo ver lista de vehiculos